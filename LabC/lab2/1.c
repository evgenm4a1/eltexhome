#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h> /* для mtrace */
#include <string.h>
#define MAX_LEN 1024 /* максимальная длина строки */
int count = 0;
int swap = 0;
int smallerLine = 0;

char** readMas(int count){
	char buffer[MAX_LEN];
	char **mas;  //указатель на массив указателей на строки
	mas = (char **)malloc(sizeof(char *)*count);// выделяем память для массива указателей
    for (int i = 0; i < count ; i++){
    	printf("Введите элемент\n");
        scanf("%s", buffer); // читаем строку в буфер
        mas[i] = (char *)malloc(sizeof(char)*strlen(buffer)); //выделяем память для строки
        strcpy(mas[i], buffer); //копируем строку из буфера в массив указателей
    }
    return mas; 
}

void line(){
	printf("Введите количество элементов\n");
	scanf("%d", &count);
}

void printMas(char **mas, int count){
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
       
    }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]); // освобождаем память для отдельной строки
    }
    free(mas); // освобождаем памать для массива указателей на строки
}

int sort (char **mas, int count){
  for (int i = count - 1; i >= 0; i--){
      for (int j = 0; j < i; j++){
	  if (strlen (mas[j]) > strlen (mas[j + 1])){
	      char *temp = mas[j];
	      mas[j] = mas[j + 1];
	      mas[j + 1] = temp;
	      swap = swap + 1;
	      smallerLine = strlen(mas[j]);
	    }
	}
  }
}

void main(int argc, char const *argv[])
{
	line();
	char **mas = NULL;
	mas = readMas (count);
    sort (mas, count);
    printMas (mas, count);
    freeMas (mas, count);
    printf ("количество перемещений = %d\n", swap);
    printf("Длина строки %d элементов\n", smallerLine);
	return 0;
}