package lab1;

public interface ICrudAction {
    void read();
    void create();
    void delete();
    void update();
}
