package lab1;

import java.util.Scanner;
import java.util.UUID;

public class Tea extends Drink {

    String packaging;

    public Tea(String name, int price, String firm, String proguctingCountry, String packaging, UUID itemID){
        super(name, price, firm, proguctingCountry,itemID);
        this.packaging = packaging;
    }
    @Override
    public void read(){
        System.out.printf("\nНазвание: %s \nЦена: %d \nФирма производитель: %s \nСтрана производитель: %s \nВид упаковки: %s\nID объекта: %s \n", name ,price ,firm,productingCountry,packaging, getItemID());
    }
    @Override
    public void delete(){
        Tea tea = new Tea("Tess", 139,"ООО Орими","Россия", "Бумажная", UUID.randomUUID());
        this.name = "null";
        this.price = 0;
        this.firm = "null";
        this.productingCountry = "null";
        this.packaging = "null";
        System.out.printf("Название: %s \nЦена: %d \nФирма производитель: %s \nСтрана производитель: %s \nВид упаковки: %s\nID объекта: %s \n", name ,price ,firm,productingCountry,packaging, getItemID());
    }
    @Override
    public void update(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите новые данные объекта");
        System.out.println("Введите название ");
        String name = scanner.next();
        System.out.println("Введите цену");
        int price = scanner.nextInt();
        System.out.println("Введите фирму производителя");
        String firm = scanner.next();
        System.out.println("Введите страну производителя");
        String productingCountry = scanner.next();
        System.out.println("Введите вид упаковки");
        String packaging = scanner.next();

        System.out.println("Объект создан");

        System.out.printf("\nНазвание: %s \nЦена: %d \nФирма производитель: %s \nСтрана производитель: %s \nВид упаковки: %s\n", name ,price ,firm, productingCountry, packaging);
    }
    @Override
    public void create(){
        Tea tea =new Tea("Tess", 139,"ООО Орими","Россия", "Бумажная", UUID.randomUUID());
        Rand rand = new Rand();
        rand.createTea();
    }

}
