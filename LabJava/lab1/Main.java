package lab1;

import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String number = args[1];
        int result = Integer.parseInt(number);

        Tea tea = new Tea("Tess", 139,"ООО Орими","Россия", "Бумажная", UUID.randomUUID());
        Coffee coffee = new Coffee("Жокей", 89,"ООО Орими","Россия", "Арабика", UUID.randomUUID());

        while (true){
            if(args[0].equals("Tea") && args[1].equals(number)){
                int i = tea.counter +result;
                System.out.println("Создано объектов: " + i);
                tea.read();

                System.out.println("\nВыбирите действия над объектом: \n1. delete (-d) \n2. update (-u) \n3. create (-c)\n");

                if(s.nextLine().equals("-d")){
                    System.out.println("Объектов создано: " + tea.counter);
                    tea.delete();
                    break;
                }else if(s.nextLine().equals("-u")){
                    tea.update();
                    break;
                }else if(s.nextLine().equals("-c")){
                    tea.create();
                    break;
                }
            }else  if(args[0].equals("Coffee") && args[1].equals(number)){
                int i = coffee.counter +result;
                System.out.println("Создано объектов: " + i);
                coffee.read();

                System.out.println("\nВыбирите действия над объектом: \n1. delete (-d) \n2. update (-u) \n3. create (-c)\n");

                if(s.nextLine().equals("-d")){
                    System.out.println("Объектов создано: " + coffee.counter);
                    coffee.delete();
                    break;
                }else if(s.next().equals("-u")){
                    coffee.update();
                    break;
                }else if(s.next().equals("-c")){
                    coffee.create();
                    break;

                }
            }
        }
    }
}
