package lab1;

import sun.misc.UUDecoder;

import java.util.UUID;

public abstract class Drink implements ICrudAction {
    String name;
    int price;
    String firm;
    String productingCountry;
    static int counter = 0;
    private UUID itemID;

    public Drink(String name, int price, String firm, String productingCountry, UUID itemID){
        this.name = name;
        this.price = price;
        this.firm = firm;
        this.productingCountry = productingCountry;
        this.itemID = itemID;
    }

    public UUID getItemID() {
        return itemID;
    }
    public void setItemID(UUID itemID) {
        this.itemID = itemID;
    }

    public void count(){
        counter = -1;
        counter++;
    }
    @Override
    public abstract void read();

    @Override
    public abstract void create();

    @Override
    public abstract void delete();

    @Override
    public abstract void update();

}
