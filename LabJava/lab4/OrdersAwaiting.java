package lab4;
import lab2.Order;
import lab2.Orders;

public class OrdersAwaiting extends ACheck {

    public OrdersAwaiting(Orders orders, Order order1, Order order2) {
        super(orders, order1, order2);
    }

    @Override
    public void run() {
        orders.buyOrder(order2);
        orders.buyOrder(order1);
        orders.showOrders();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        orders.checkOrders(order1);
        orders.showOrders();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
