package lab4;

import lab2.Order;
import lab2.Orders;

public class OrdersProcessed extends ACheck {

    public OrdersProcessed(Orders orders, Order order1, Order order2) {
        super(orders, order1, order2);
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        orders.deleteOrders(order1);
        orders.showOrders();
    }
}
