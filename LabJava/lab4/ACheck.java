package lab4;

import lab2.Order;
import lab2.Orders;

public abstract class ACheck implements Runnable {

     public Orders orders;
     public Order order1;
     public Order order2;

     public ACheck(Orders orders, Order order1, Order order2){
         this.orders = orders;
         this.order1 = order1;
         this.order2 = order2;
     }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Order getOrder1() {
        return order1;
    }

    public void setOrder1(Order order1) {
        this.order1 = order1;
    }

    public Order getOrder2() {
        return order2;
    }

    public void setOrder2(Order order2) {
        this.order2 = order2;
    }
}
