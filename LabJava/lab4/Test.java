package lab4;

import lab2.Order;
import lab2.Orders;

import java.lang.reflect.AccessibleObject;

public class Test extends Thread {

    public static void main(String[] args) throws InterruptedException {
        Orders orders = new Orders();
        Order order1 = new Order();
        Order order2 = new Order();

        OrdersProcessed ordersProcessed = new OrdersProcessed(orders, order1, order2);
        OrdersAwaiting ordersAwaiting = new OrdersAwaiting(orders, order1, order2);

        Thread processed = new Thread(ordersProcessed);
        Thread awaiting = new Thread(ordersAwaiting);

        awaiting.start();
        sleep(5000);
        processed.start();
    }
}
