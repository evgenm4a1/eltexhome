package lab2;
import lab1.Coffee;
import lab1.Tea;
import java.util.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ShoppingCart shoppingCart = new ShoppingCart();
        Order order1 = new Order();
        Order order2 = new Order();
        Orders orders = new Orders();

        Tea tea = new Tea("Tess", 139,"ООО Орими","Россия", "Бумажная", UUID.randomUUID());
        Coffee coffee = new Coffee("Жокей", 89,"ООО Орими","Россия", "Арабика", UUID.randomUUID());

        System.out.println("Добавляем объект в корзину/Показать корзину");
        shoppingCart.add(tea);
        shoppingCart.addID(tea.getItemID());
        shoppingCart.showCart();

        System.out.println("\nУдаление объекта");
        shoppingCart.delete(tea);
        shoppingCart.showCart();

        System.out.println("\nИщем объект в корзине\nЧай есть в корзине?");
        shoppingCart.searchInCart(tea.getItemID());
        System.out.println("Кофе есть в корзине?");
        shoppingCart.searchInCart(coffee.getItemID());

        System.out.println("\nПо времени создания");
        orders.buyOrder(order2);
        orders.checkOrders(order2);
        orders.buyOrder(order1);
        orders.showOrdersBuyTime();

        System.out.println("\nУдаление");
        orders.deleteOrders(order2);
        orders.showOrdersBuyTime();




    }

}