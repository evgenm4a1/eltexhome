package lab2;
import java.util.UUID;

public class Credentials {

    private UUID id = UUID.randomUUID();
    private String surname;
    private String name;
    private String middleName;
    private String email;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void readOfcredentials(){
       Randcrred randcrred = new Randcrred();
       randcrred.dataOfcredentials();
    }

}
