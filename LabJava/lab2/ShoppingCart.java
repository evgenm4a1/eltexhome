package lab2;
import lab1.Drink;
import java.util.*;

public class ShoppingCart<T extends Drink>{

    private Set<UUID> idDrinks;
    private List<T> drinks;

    public ShoppingCart(){
        drinks = new ArrayList<>();
        idDrinks = new HashSet<UUID>();
    }
    public ShoppingCart(List<T> drinks){
        this.drinks = drinks;
    }
    public ShoppingCart(Set<UUID> idDrinks){
        this.idDrinks = idDrinks;
    }
    public boolean add(T drink){
        return drinks.add(drink);
    }
    public boolean addID(UUID id){
        return idDrinks.add(id);
    }
    public boolean delete(T drink){
        return drinks.remove(drink);
    }
    public void randomCart(ShoppingCart shoppingCart){
        Randcrred randcrred = new Randcrred();
        randcrred.randomAddToCart(shoppingCart);
    }
    public void searchInCart(UUID id){
        System.out.println(idDrinks.contains(id));
    }
    public void deleteCartAndID(){
        drinks.removeAll(drinks);
        idDrinks.removeAll(idDrinks);
    }
    public void showCart(){
        for (T e: drinks){
            for(UUID a: idDrinks){
                System.out.println(e);
                System.out.println(a);
            }
        }
    }
}
