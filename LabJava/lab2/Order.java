package lab2;
import java.util.Date;

public class Order {

    private Credentials cred;
    private ShoppingCart cart;
    private Date timeIsOver;
    private StatusOrder statusOrder;
    private Date timeOfcreation = new Date(System.currentTimeMillis());
    private int waitingTimeInMinutes = 1;

    public enum StatusOrder {
        AWAITING, PROCESSED
    }
    public Order() {
        statusOrder = StatusOrder.AWAITING;
        waitingTimeInMinutes = 1;
    }

    public StatusOrder getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(StatusOrder statusOrder) {
        this.statusOrder = statusOrder;
    }

    public Date getTimeIsOver() {
        return timeIsOver;
    }

    public void setTimeIsOver(Date timeIsOver) {
        timeIsOver.setTime(2221212315235L);
        this.timeIsOver = timeIsOver;
    }

    public Credentials getCred() {
        return cred;
    }
    public void setCred(Credentials cred) {
        this.cred = cred;
    }
    public ShoppingCart getCart() {
        return cart;
    }
    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }
    public Date getTimeOfcreation(){
        return timeOfcreation;
    }
    public void setTimeOfcreation(Date timeOfcreation) {
        this.timeOfcreation = timeOfcreation;
    }
    public void infoCred(Credentials cred) {
        cred.readOfcredentials();
    }
    public void infoOrder(ShoppingCart cart) {
        cart.randomCart(cart);
        cart.showCart();
    }
    public void myOrder(){
        System.out.println("\nСтатус: " + statusOrder.name());
        System.out.println("Создано: " + timeOfcreation.getTime());
        System.out.println("Время ожидания: " + waitingTimeInMinutes);
        infoCred(new Credentials());
        System.out.println("Заказ:");
        infoOrder(new ShoppingCart());
    }
}