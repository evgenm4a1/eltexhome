package lab2;
import java.util.*;

public class Orders<T extends Order>{

    private Map<Date, T> localDate;
    private List<T> orders;

    public Orders(){
        orders = new LinkedList<>();
        localDate = new HashMap<Date, T>();
    }
    public Orders(Map<Date, T> localDate){
        this.localDate = localDate;
    }
    public Orders(List<T> orders){
        this.orders = orders;
    }
    public boolean buy(T order){
        return orders.add(order);
    }
    public void buyTime(T order){
        localDate.put(order.getTimeOfcreation(), order);
    }
    public void buyOrder(T order){
        Date date = new Date();
        order.setTimeIsOver(date);
        buy(order);
        buyTime(order);
    }
    public void checkOrders(T order) {
        localDate.entrySet().removeIf(pair -> {
            if (pair.getValue().getStatusOrder() == Order.StatusOrder.AWAITING && check(order, pair)) {
                order.setStatusOrder(Order.StatusOrder.PROCESSED);
            }
            return false;
        });
    }
    public void deleteOrders(T order){
        localDate.remove(order.getTimeOfcreation());
    }
    public boolean check(T order, Map.Entry<Date, T> pair){
        return order.getTimeOfcreation().before(pair.getValue().getTimeIsOver());
    }
    public void showOrdersBuyTime(){
        for(Map.Entry<Date, T> item: localDate.entrySet()){
            item.getValue().myOrder();
        }
    }
    public void showOrders(){
        orders.forEach(T::myOrder);
    }
}

