package lab2;
public class Test{

    public static void main(String args[]) throws InterruptedException {

        Order order1 = new Order();
        Order order2 = new Order();
        Orders orders = new Orders();

        orders.buyOrder(order1);
        orders.buyOrder(order2);
        orders.checkOrders(order1);
        orders.showOrdersBuyTime();

    }
}

